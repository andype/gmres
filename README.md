# GMRES 
[https://en.wikipedia.org/wiki/Generalized_minimal_residual_method](https://en.wikipedia.org/wiki/Generalized_minimal_residual_method)

GMRES implementation I did ages ago with help from one of my professors.

(Approximately solves the matrix by leaving only minimal residues.)