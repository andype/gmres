#include <cstdio>
#include <cstdlib>
#include <iostream>
#include <cmath>
#include <complex>
#include <complex.h>
#include <fftw3.h>

#define M3(a) ((a).m[k + (a).zdim * (j + (a).ydim * i)])
#define M2(a) ((a).m[j + (a).ydim * i])
#define M1(a) ((a).m[i])
#define MAT(a, i, j) ((a)[(j) + (a).ydim * (i)])
#define MATR(a, i, j) ((a).r[(j) + (a).ydim * (i)])
#define MATC(a, i, j) ((a).m[(j) + (a).ydim * (i)])
#define MMR(a, i, j, k) ((a).r[k + (a).zdim * ((j) + (a).ydim * (i))])
#define MMC(a, i, j, k) ((a).m[k + (a).zdim * ((j) + (a).ydim * (i))])
#define VECR(a, i) ((a).r[(i)])
#define VECC(a, i) ((a).m[(i)])


struct mat{ fftw_complex * m; double * r; int xdim ; int ydim ; int zdim; bool comp;};

void here (std::string loc) {
  std::cerr << loc << "here" << std::endl;
}
void here (int loc) {
  std::cerr << loc << "here" << std::endl;
}
void here (double loc) {
  std::cerr << loc << "here" << std::endl;
}

mat mkmat (int xdim, int ydim, int zdim, bool comp) {
  mat rv;
  if (comp) {
    rv.m = (fftw_complex *)fftw_malloc(sizeof(fftw_complex) * xdim * ydim * zdim);
  } else {
    rv.r = (double *)malloc(sizeof(double) * xdim * ydim * zdim);
  }
  rv.comp = comp;
  rv.xdim = xdim;
  rv.ydim = ydim;
  rv.zdim = zdim;
  return rv;
}

mat zeros (int xdim, int ydim, int zdim, bool comp) {
  mat rv = mkmat(xdim, ydim, zdim, comp);
  if (comp) {
    for (int i = 0 ; i < xdim ; i++) {
      for (int j = 0 ; j < ydim ; j++) {
        for (int k = 0 ; k < zdim ; k++) {
          MMC(rv, i, j, k) = 0;
        }
      }
    }
  } else {
    for (int i = 0 ; i < xdim ; i++) {
      for (int j = 0 ; j < ydim ; j++) {
        for (int k = 0 ; k < zdim ; k++) {
          MMR(rv, i, j, k) = 0;
        }
      }
    }
  }
  return rv;
}

//Load matrix from file
mat lm (std::string fname, bool comp) {
  FILE * fp = NULL;
  if (fp == NULL) {
    fp = fopen(fname.c_str(),"r");
  }
  if (fp == NULL) {
    std::cerr << "E:lm" << fname << std::endl;
    exit(-1);
  }
  int rows = 0;
  int cols = 0;
  char a[2] = "\n" ;
  bool numflag = false;
  //try to find number of columns by looking for sequences of whitespace-digit in the first row
  while (!feof(fp)) {
    fscanf(fp,"%c", &a[0]);
    if (a[0] == '\n') {
      break;
    }
    if (!numflag && atoi(a)) {
      cols++;
      numflag = true;
    } else if (a[0] == ' ') {
      numflag = false;
    } else if (a[0] == '0' && rows == 0) {
      cols++;
      numflag = true;
    }
    rows++;
  }
  rows = 0;
  //try to find number of rows
  fseek(fp,0,0);
  while (!feof(fp)) {
    fscanf(fp, "%c", &a[0]);
    if (a[0] == '\n') {
      rows++;
    }
  }
  rows--;
  if (rows < 1 || cols < 1) {
    std::cerr << "E:lm dims" << std::endl;
  }

  //malloc the matrix
  mat rv = mkmat(rows, cols, 1, comp);
  rv.xdim = rows;
  rv.ydim = cols;
  rv.zdim = 1;
  double in;

  //now scan in the values
  fseek(fp,0,0);
  if (comp) {
    for (int i = 0 ; i < rv.xdim ; i++) {
      for (int j = 0 ; j < rv.ydim ; j++) {
        fscanf(fp, "%lf", &in);
        MATC(rv, i, j) = in;
      }
    }
  } else {
    for (int i = 0 ; i < rv.xdim ; i++) {
      for (int j = 0 ; j < rv.ydim ; j++) {
        fscanf(fp, "%lf", &MATR(rv, i, j));
      }
    }
  }

  printf("loaded: %s %dx%d\n", fname.c_str(), rv.xdim, rv.ydim);

  fclose(fp);
  fp = NULL;
  return rv;
}

void fpm (mat rv, std::string fname, bool cflag) {
  FILE * fp2 = NULL;
  if (fp2 ==NULL)
    fp2 = fopen(fname.c_str(),"w");
  if (fp2 == NULL) {
    printf("E:fpm");
    exit(-1);
  }

  for (int i = 0 ; i < rv.xdim ; i++) {
    for (int j = 0 ; j < rv.ydim ; j++) {
      if (rv.comp) {
        if (cflag) {
          fprintf(fp2, "%16.16lf ", cimag(MATC(rv, i, j)));
        } else {
          fprintf(fp2, "%16.16lf ", creal(MATC(rv, i, j)));
        }
      } else {
        fprintf(fp2, "%16.16lf ", MATR(rv, i, j));
      }
    }
    fprintf(fp2, "\n");
  }
  //here(8);
  fclose(fp2);
  fp2 = NULL;
}

void ppm (mat rv, bool cflag) {
  fpm(rv, "tmpscrap", cflag);
  system("lua gma4th.lua -load tmpscrap");
}
void here (mat loc) {
  std::cerr <<"here" << std::endl;
  ppm(loc, 0);
  int a;
  scanf("%d",&a);
}


mat mult (mat A, mat B) {
  mat rv = mkmat(A.xdim, B.ydim, 1, A.comp || B.comp);
  if (rv.comp) {
    for (int i = 0 ; i < A.xdim ; i++) {
      for (int j = 0 ; j < B.ydim ; j++) {
        MATC(rv, i, j) = 0;
        for (int k = 0 ; k < A.ydim ; k++) {
          MATC(rv, i, j) += MATC(A, i, k) * MATC(B, k, j);
        }
      }
    }
  } else {
    for (int i = 0 ; i < A.xdim ; i++) {
      for (int j = 0 ; j < B.ydim ; j++) {
        MATR(rv, i, j) = 0;
        for (int k = 0 ; k < A.ydim ; k++) {
          MATR(rv, i, j) += MATR(A, i, k) * MATR(B, k, j);
        }
      }
    }
  }
  return rv;
}
        



mat copy (mat in) {
  mat rv = mkmat(in.xdim, in.ydim, in.zdim, in.comp);
  if (in.comp) {
    for (int i = 0 ; i < in.xdim ; i++) {
      for (int j = 0 ; j < in.ydim ; j++) {
        for (int k = 0 ; k < in.zdim ; k++) {
          MMC(rv, i, j, k) = MMC(in, i, j, k);
        }
      }
    }
  } else {
    for (int i = 0 ; i < in.xdim ; i++) {
      for (int j = 0 ; j < in.ydim ; j++) {
        for (int k = 0 ; k < in.zdim ; k++) {
          MMR(rv, i, j, k) = MMR(in, i, j, k);
        }
      }
    }
  }
  return rv;
}

double calc_res (mat A, mat b, mat H, double h0n1, int k, int m, mat q, mat x0, int max_it) {
  k++;
  mat xk = mkmat(m + 1, 1, 1, 0);
  mat y = mkmat(max_it + 2, 1, 1, 0);
  mat h = copy(H);
  xk.r[0] = h0n1;
  for (int i = 1 ; i <= k + 2 ; i++) {
    xk.r[i] = 0;
  }

  double c = 1; 
  double s = 0; 
  double tau1 = 0;
  double tau2 = 0;
  int lsk = 0;

  for (int j = 0 ; j <= k - 1; j++) {
    if (MATR(h, j + 1, j) == 0) {
      c = 1;
      s = 0;
    } else {
      if (fabs(MATR(h, j + 1, j)) > fabs(MATR(h, j, j))) {
        tau1 = -1 * MATR(h, j, j) / MATR(h, j + 1, j);
        //tau1 = 0;
        s = 1.0 / sqrt(1.0 + tau1 * tau1);
        c = s * tau1;
      } else {
        tau1 = -1 * MATR(h, j + 1, j) / MATR(h, j, j);
        c = 1.0 / sqrt(1.0 + tau1 * tau1);
        s = c * tau1;
      }
    }
    //applying the givens rotations
    for (int lsk = j ; lsk <= k - 1 ; lsk++) {
      tau1 = c * MATR(h, j, lsk) - s * MATR(h, j + 1, lsk);
      tau2 = s * MATR(h, j, lsk) + c * MATR(h, j + 1, lsk);
      MATR(h, j, lsk) = tau1;
      MATR(h, j + 1, lsk) = tau2;
    }
    tau1 = c * xk.r[j] - s * xk.r[j + 1];
    tau2 = s * xk.r[j] + c  *xk.r[j + 1];
    xk.r[j] = tau1;
    xk.r[j + 1] = tau2;
  }

  for (int i = k - 1 ; i >= 0 ; i--) {
    y.r[i] = xk.r[i];
    for (int j = i + 1 ; j < k ; j++) {
      y.r[i] -= y.r[j] * MATR(h, i, j);
    }
    y.r[i] /= MATR(h, i, i);
  }

  //ppm(x0);

  for (int i = 0 ; i < m ; i++) {
    xk.r[i] = x0.r[i];
    for (int j = 0 ; j <= k ; j++) {
      xk.r[i] += MATR(q, i, j) * y.r[j];
    }
  }
  
  tau1 = 0;
  tau2 = 0;

  y = mult(A, xk);
  for (int i = 0 ; i < m ; i++) {
    tau2 = b.r[i] - y.r[i];
    tau1 += tau2 * tau2;
  }
  tau1 = sqrt(tau1);

  return tau1;
}

mat gmres (mat A, mat b, mat x0, int m, int max_it) {

  int k = 0;
  int lsk = 0;
  double lshi = 0;

  mat xk = mkmat(m + 2, 1, 1, 0);

  mat r = mult(A, b);

  for (int i = 0 ; i < m ; i++) {
    VECR(r,i) = VECR(b,i) - VECR(r,i);
  }

  mat h = zeros(max_it + 3, max_it + 2, 1, 0);

  double h0n1 = 0;
  for(int i = 0 ; i < m ; i++){
    h0n1 += VECR(r,i) * VECR(r,i);
	}
	h0n1 = sqrt(h0n1);

  //orthonormal basis
  mat q = zeros(m + 2, max_it + 2, 1, 0);

  //least squares solution
  mat y = zeros(max_it + 2, 1, 1, 0);

  double c = 0, s = 0, tau1 = 0, tau2 = 0;
  k = -1;

  for (int i = 0 ; i < m ; i++) {
    MATR(q, i, k + 1) = VECR(r,i) / h0n1;
  }

  k++;

  
  for (int i = 0 ; i < m ; i++) {
    VECR(r,i) = 0;
    for (int j = 0 ; j < m ; j++) {
      VECR(r,i) += MATR(A, i, j) * MATR(q, j, k);
    }
  }
  for (int i = 0 ; i <= k ; i++) {
    MATR(h, i, k) = 0;
    for (int j = 0 ; j < m ; j++) {
      MATR(h, i, k) += VECR(r,j) * MATR(q, j, i);
    }
    for (int j = 0 ; j < m ; j++) {
      VECR(r, j) -= MATR(h, i, k) * MATR(q, j, i);
    }
  }

  MATR(h, k+1, k) = 0;
  for (int j = 0 ; j < m ; j++) {
    MATR(h, k+1, k) += VECR(r, j) * VECR(r, j);
  }
  MATR(h, k+1, k) = sqrt(MATR(h, k+1, k));

  std::cout << "norm of the residual = " << calc_res(A,b,h,h0n1, k, m, q, x0, max_it) << std::endl;

  //made to main loop
  while (k < m - 1 && k < max_it - 1 && fabs(MATR(h, k + 1, k)) > 1e-16) {
    //std::cout << k << std::endl;

    for (int i = 0 ; i < m ; i++) {
      MATR(q, i, k + 1) = VECR(r,i) / MATR(h, k + 1, k);
    }
    //here(2);
    
    k++;

    for (int i = 0 ; i < m ; i++) {
      VECR(r,i) = 0;
      for (int j = 0 ; j < m ; j++) {
        VECR(r,i) += MATR(A, i, j) * MATR(q, j, k);
      }
    }

    for (int i = 0 ; i <= k ; i++) {
      MATR(h, i, k) = 0;

      for (int j = 0 ; j < m ; j++) {
        MATR(h, i, k) += VECR(r,j) * MATR(q, j, i);
      }

      for (int j = 0 ; j < m ; j++) {
        VECR(r, j) -= MATR(h, i, k) * MATR(q, j, i);
      }
    }

    //here(1);
    MATR(h, k + 1, k) = 0;
    for (int j = 0 ; j < m ; j++) {
      MATR(h, k + 1, k) += VECR(r, j) * VECR(r, j);
    }
    MATR(h, k + 1, k) = sqrt(MATR(h, k + 1, k));  

    std::cout << "norm of the residual = " << calc_res(A,b,h,h0n1, k, m, q, x0, max_it) << std::endl;

  //end main loop
  }
  //here(3);

  VECR(xk, 0) = h0n1;
  for (int i = 1 ; i <= k + 2 ; i++) {
    VECR(xk, i) = 0;
  }
  k++;

  for (int j = 0 ; j <= k - 1 ; j++) {
    if (MATR(h, j + 1, j) == 0) {
      c = 1;
      s = 0;
    } else {
      if (fabs(MATR(h, j + 1, j)) > fabs(MATR(h, j, j))) {
        tau1 = -1 * MATR(h, j, j) / MATR(h, j + 1, j);
        s = 1.0 / sqrt(1.0 + tau1 * tau1);
        c = s * tau1;
      } else {
        tau1 = -1 * MATR(h, j + 1, j) / MATR(h, j, j);
        c = 1.0 / sqrt(1.0 + tau1 * tau1);
        s = c * tau1;
      }
    }
    //givens mult
    for (lsk = j ; lsk <= k - 1 ; lsk++) {
      tau1 = c * MATR(h, j, lsk) - s * MATR(h, j + 1, lsk);
      tau2 = s * MATR(h, j, lsk) + c * MATR(h, j + 1, lsk);
      MATR(h, j, lsk) = tau1;
      MATR(h, j + 1, lsk) = tau2;
    }
    tau1 = c * VECR(xk, j) - s * VECR(xk, j + 1);
    tau2 = s * VECR(xk, j) + c * VECR(xk, j + 1);
    VECR(xk, j) = tau1;
    VECR(xk, j + 1) = tau2;
  }
  for (int i = k - 1 ; i >= 0 ; i--) {
    VECR(y, i) = VECR(xk, i);
    for (int j = i + 1 ; j < k ; j++) {
      VECR(y, i) -= VECR(y, j) * MATR(h, i, j);
    }
    VECR(y, i) /= MATR(h, i, i);
  }
  //here(4);

  for (int i = 0 ; i < m ; i++) {
    VECR(xk, i) = VECR(x0, i);
    for (int j = 0 ; j <= k ; j++) {
      VECR(xk, i) += MATR(q, i, j) * VECR(y, j);
    }
  }

  free(r.r);
  free(h.r);
  free(q.r);
  free(y.r);

  //here(5);
  r.r = NULL;
  h.r = NULL;
  q.r = NULL;
  y.r = NULL;

  xk.xdim = m;

  return xk;
}



mat ones (int xdim, int ydim, int zdim, bool comp) {
  mat rv = mkmat(xdim, ydim, zdim, comp);
  if (comp) {
    for (int i = 0 ; i < xdim ; i++) {
      for (int j = 0 ; j < ydim ; j++) {
        for (int k = 0 ; k < zdim ; k++) {
           MMC(rv, i, j, k) = 1;
        }
      }
    }
  } else {
    for (int i = 0 ; i < xdim ; i++) {
      for (int j = 0 ; j < ydim ; j++) {
        for (int k = 0 ; k < zdim ; k++) {
           MMR(rv, i, j, k) = 1;
        }
      }
    }
  }

  return rv;
}

//rewrite of fft
mat fft_1_0 (mat v) {
  int t1 = 2;
  int t2 = 3;
  mat F1 = mkmat(t1 - 1, t2 - 1, 1, 1);
  mat F2 = mkmat(t1 - 1, t2 - 1, 1, 1);
  mat rv = zeros(v.xdim, 1, 1, 1);
  int count = 0;

  fftw_complex wN = cexp(2 * M_PI * I / v.xdim);

  for (int j1 = 0 ; j1 < t1 ; j1++) {
    for (int n2 = 0 ; n2 < t2 ; n2++) {
      MATC(F1, j1, n2) = 0;
      for (int n1 = 0 ; n1 < t1 ; n1++) {
        MATC(F1, j1, n2) += VECR(v, n2 + t2 * n1) * cpow(wN, j1 * t2 * n1);
        count++;
      }
    }
  }

  v.m = (fftw_complex *)fftw_malloc(v.xdim * sizeof(fftw_complex));
  v.comp = true;
  for (int j1 = 0 ; j1 < t1 ; j1++) {
    for (int j2 = 0 ; j2 < t2 ; j2++) {
      MATC(F2, j1, j2) = 0;
      for (int n2 = 0 ; n2 < t2 ; n2++) {
        //printf("%d %d\n",j1 * n2 + t1 * j2 * n2, j1 + t1 * j2);
        MATC(F2, j1, j2) += MATC(F1, j1, n2) * cexp(2*M_PI * I * (j1 * n2 + t1 * j2 * n2) / v.xdim);
      }
      VECC(rv, j1 + t1 * j2) = MATC(F2, j1, j2);
    }
  }

  return rv;
}



int main (void) {
  mat A = lm("hilb100", 0);
  mat b= lm("b", 0);
  ppm(b,0);
  mat x0 = ones(A.xdim, 1, 1, 0);
  //ppm(A,0);
  int w = 1;

  switch (w) {
    case 1:
      b = ones(A.xdim, 1, 1, 0);
      x0 = gmres(A, b, x0, b.xdim, b.xdim);
      break;
    case 2:
      x0 = fft_1_0(b);
      break;
  }

  ppm(x0,0);

  return 0;
}
